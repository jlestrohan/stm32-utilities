/*
 * lcd_service.c
 *
 *  Created on: Mar 25, 2020
 *      Author: Jack Lestrohan
 *
 *      Feel free to use this file
 *      Please keep credits in this header
 */

#include <stdlib.h>
#include <string.h>
#include <FreeRTOS.h>
#include "lcd_service.h"
#include "freertos_logger_service.h"
#include <string.h>
#include <stdio.h>

#define DATA_BUFFER		16	/* size of the queue buffer */
#define MSG_BUFFER		10

I2C_HandleTypeDef *_hi2cHandler;
typedef StaticTask_t osStaticThreadDef_t;
typedef StaticQueue_t osStaticMessageQDef_t;

typedef enum {
	lcdServiceNotInit,
	lcdServiceInitOK,
	lcdServiceInitError
} lcdServiceStatus_t;
lcdServiceStatus_t lcdServiceStatus = lcdServiceInitError;

typedef struct {
	char text[DATA_BUFFER];
} MSGQUEUE_OBJ_t;
MSGQUEUE_OBJ_t msg;

uint8_t queue_lcdBuffer[ MSG_BUFFER * sizeof( MSGQUEUE_OBJ_t ) ];
osStaticMessageQDef_t queue_lcdControlBlock;
osMessageQueueId_t queue_lcdHandle;

/**
 * Definitions for lcdServiceTask
 */
osThreadId_t lcdServiceTaHandle;
uint32_t lcdServiceTaBuffer[ 256 ];
osStaticThreadDef_t lcdServiceTaControlBlock;
const osThreadAttr_t lcdServiceTa_attributes = {
		.name = "lcdServiceTask",
		.stack_mem = &lcdServiceTaBuffer[0],
		.stack_size = sizeof(lcdServiceTaBuffer),
		.cb_mem = &lcdServiceTaControlBlock,
		.cb_size = sizeof(lcdServiceTaControlBlock),
		.priority = (osPriority_t) osPriorityLow,
};

osStatus_t status;

void lcdService_task(void *argument);

/**
 * Initialize lcd
 */
uint8_t lcd_service_init (I2C_HandleTypeDef *hi2c)
{
	_hi2cHandler = hi2c;
	queue_lcdHandle = osMessageQueueNew (MSG_BUFFER, sizeof(MSGQUEUE_OBJ_t), NULL);

	/* creation of LoggerServiceTask */
	lcdServiceTaHandle = osThreadNew(lcdService_task, NULL, &lcdServiceTa_attributes);
	if (!lcdServiceTaHandle) {
		lcdServiceStatus = lcdServiceInitError;
		loggerE("LCD Service - Initialization Failure");
		return (EXIT_FAILURE);
	}

	loggerI("LCD Service - Initialization complete");
	return (EXIT_SUCCESS);
}

void lcdService_task(void* argument)
{
	/* LCD INITIALIZATION */
	/* 4 bit initialisation */
	osDelay(50);;  /* wait for >40ms */
	lcd_send_cmd (0x30);
	osDelay(5);;  /* wait for >4.1ms */
	lcd_send_cmd (0x30);
	osDelay(1);  /* wait for >100us */
	lcd_send_cmd (0x30);
	osDelay(10);
	lcd_send_cmd (0x20);  /* 4bit mode */
	osDelay(10);

	/* dislay initialization */
	lcd_send_cmd (0x28); /* Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters) */
	osDelay(1);
	lcd_send_cmd (0x08); /* Display on/off control --> D=0,C=0, B=0  ---> display off */
	osDelay(1);
	lcd_send_cmd (0x01);  /* clear display */
	osDelay(2);
	lcd_send_cmd (0x06); /* Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift) */
	osDelay(1);
	lcd_send_cmd (0x0C); /* Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits) */

	lcd_send_cmd(0x80);
	osDelay(10);
	lcd_send_string("This is a LCD");

	for (;;)
	{

		/* gets every char it finds in the queue and then sends it over */
		osMessageQueueGet(queue_lcdHandle, &msg, NULL, osWaitForever);   // wait for message
		loggerI(msg.text);
		int i=0;
		while (*(msg.text+i) != '\0') {       /* Stop looping when we reach the null-character. */
			lcd_send_data(*(msg.text+i));    /* Print each character of the string. */
		    i++;
		}

		osDelay(100);
	}
}

/**
 * Send command to the LCD
 * @param cmd
 */
void lcd_send_cmd (char cmd)
{
	char data_u, data_l;
	uint8_t data_t[4];
	data_u = (cmd&0xf0);
	data_l = ((cmd<<4)&0xf0);
	*(data_t) = data_u|0x0C;  /* en=1, rs=0 */
	*(data_t+1) = data_u|0x08;  /* en=0, rs=0 */
	*(data_t+2) = data_l|0x0C;  /* en=1, rs=0 */
	*(data_t+3) = data_l|0x08;  /* en=0, rs=0 */
	HAL_I2C_Master_Transmit (_hi2cHandler, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

/**
 * Send data to the LCD
 * @param data
 */
void lcd_send_data (char data)
{
	char data_u, data_l;
	uint8_t data_t[4];
	data_u = (data&0xf0);
	data_l = ((data<<4)&0xf0);
	*(data_t) = data_u|0x0D;  /* en=1, rs=0 */
	*(data_t+1) = data_u|0x09;  /* en=0, rs=0 */
	*(data_t+2) = data_l|0x0D;  /* en=1, rs=0 */
	*(data_t+3) = data_l|0x09;  /* en=0, rs=0 */
	HAL_I2C_Master_Transmit (_hi2cHandler, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

/**
 * Send string to the LCD
 * @param str
 */
void lcd_send_string (char *str)
{
	/* pass on that string to our lcdQueue so it gets processed the FreeRTOS way */
	memcpy(msg.text, str, DATA_BUFFER);
	osMessageQueuePut(queue_lcdHandle, &msg, 0U, osWaitForever);
}

/**
 * Put cursor at the entered position row (0 or 1), col (0-15)
 * @param row
 * @param col
 */
void lcd_put_cur(int row, int col)
{
	switch (row)
	{
	case 0:
		col |= 0x80;
		break;
	case 1:
		col |= 0xC0;
		break;
	default:
		break;
	}

	lcd_send_cmd (col);
}


/**
 * Clear the LCD
 */
void lcd_clear (void)
{
	lcd_send_cmd (0x80);
	for (int i=0; i<70; i++)
	{
		lcd_send_data (' ');
	}
}




