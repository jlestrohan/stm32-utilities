/*
 * IRQ_Handler.c
 *
 *  Created on: Feb 26, 2020
 *      Author: jack
 */

#include "main.h"
#include "button_handler.h"
#include "cmsis_os2.h"
#include "usart.h"
#include <string.h>

static osStatus_t osStatus;

/**
 *
 * @param GPIO_Pin
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == B1_Pin) {
		/* raises an event flag to let the module that the button was pressed */
		osStatus = osEventFlagsSet(evt_usrbtn_id, BTN_PRESSED_FLAG);
	}
}

/**
 *
 * @param huart
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
	char *msg = "char hello";
	HAL_UART_Transmit_IT(huart, (uint8_t*) msg, strlen(msg));
	//cmd_parse_uart_cb();
}
