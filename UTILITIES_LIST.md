## Utilities List

- In order to use these different utilities you can drop these files into your project as is.
- By default all peripheral initializations from the IoC were made by generating separate .h/.c files. Plan accordingly if you prefer having a single main.c file generated (probably a few includes to be updated).
- The main project was compiled for a Nucleo G474RE board. 
 

#### FreeRTOS Logger Service
files: 

	Inc/freertos_logger_service.h
	Src/freertos_logger_service.c
	
	
#### FreeRTOS Button Handler (debouncing)
files:

    Inc/button_handler.h
    Src/button_handler.c

#### FreeRTOS Command Parser
work in progress

#### FreeRTOS Led Driver
work in progress



