# STM32 FreeRTOS Utilities

***

- Set of utilities to be used with STM32CubeMX
- work heavily in progress.
- All useable files are merged into the master branch.
- All devs are to be made into the dev (and child) branches

- The main project was compiled for a Nucleo G474RE board
- You can just use the spare files corresponding to the utility you need independantly of the project if you wish (see UTILITIES.md)

***


## FreeRTOS Logger Service: 
- A full featured logger to be used within your application. It is actually working quite well and requires a very few configuration steps to integrate to any of your applications.

#### How to install:
1. drop both files: ***freertos_logger_service.h*** and **freertos_logger_service.c** into your "Inc" and "Src" directories, or anywhere suits you according to your project settings and include paths

2. call :

		log_initialize(&your_uart_instance)

 in the early stage start of your application initialization (in the *app_freertos.c *at the very begining of the MX_FREERTOS_Init function - under USER CODE BEGIN Init - for instance is quite a good spot):

	void MX_FREERTOS_Init(void) {
	  /* USER CODE BEGIN Init */
		log_initialize(&huart2); /* <------------------ GOOD SPOT
		
		/** more code fromhere ... */
	  /* USER CODE END Init */
	    /* USER CODE BEGIN RTOS_MUTEX */
  	/* add mutexes, ... */
  	/* USER CODE END RTOS_MUTEX */
  	
	  /** more code here again...  etc...*/



3. **Use the logger macros where it suits you in the program. 
for instance.. 
				
		   
		  /* USER CODE BEGIN StartDefaultTask */
			loggerI("Starting default task ...");
		  /* Infinite loop */
		  for(;;)
		  {

		  

For now the logger only outputs to the serial console, thru the UART instance that you pass to the Init routine. It creates its very own queue and task (lowest priority) and will do it's job in complete autonomy.

A mutex is exposed in the header file that is used by the service to access the UART:

		osMutexId_t mutex_loggerService_Hnd;

There are actually five log priorities and the same number of macros, just use the one accordingly to the message you're willing to output. 

	loggerV("message") -> VERBOSE
	loggerI("message") -> INFO
	loggerW("message") -> WARNING
	loggerE("message") -> ERROR
	loggerA("message") -> ALERT

These macros are shortcuts for the exposed function:

	void log_service(char *log_msg, LogPriority priority);

If you have any questions or if you want to be a part of this project, please feel free to contribute.

## FreeRTOS Button Handler: 

A button debouncer that raises an osEvent (evt_usrbtn_id) that you can use in your tasks as such:

	    flags = osEventFlagsWait(evt_id, evt_usrbtn_id, osFlagsWaitAny, osWaitForever);
	    //handle event
  
- drop the .h/.c files into your project
- include "button_handler.h"
- in your app_freertos.c call:

		buttonService_initialize();

- don(t forget to setup the osEvenFlag in your HAL Rising/Falling callback to make sure that the button task is woken up..


		void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
		{
			if (GPIO_Pin == B1_Pin) {
				/* raises an event flag to let the module that the button was pressed */
				osStatus = osEventFlagsSet(evt_usrbtn_id, BTN_PRESSED_FLAG);
			}
		}
- Feel free to modify the code to what suits you best, especially button Debouncer time for instance. The example task only toggles the onboard LED on and off for now.

 ***



*You are free to use this code in any of your non commercial projects*

## 16x2 LCD Service: 
A simple freertos controller for a 16x2 I2C LCD screen. There's still some ongoing work on this one but it is perfectly useable "as is" for now.

#### How to install:
* drop these two files in the relevant directories:

		lcd_service.h
		lcd_service.c

* define your device slave's address. Please mind the 7 bits to 8 bits conversion in case of using some Arduino I2C stuff as the address may change.

		#define SLAVE_ADDRESS_LCD 0x27 << 1

* finally initialize the service where it suits you best, passing your i2c instance to the initializer...

		lcd_service_init(&hi2c1);

*(need a few more edits, usage explanations here, check lcd_service.h for a sumup of the functionalities for now)*

##Command Parser Service: 
This utility is actually under heavy construction...
